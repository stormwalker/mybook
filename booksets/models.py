from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models

# Create your models here.
from requests import get


class Bookset(models.Model):
    mybook_id = models.IntegerField(primary_key=True)
    mybook_json = JSONField()

    @property
    def json(self):
        return self.mybook_json

    @classmethod
    def from_mybook_id(cls, mybook_id):
        query = cls.objects.filter(mybook_id=mybook_id)
        if query.exists():
            return query.first()
        response = get(f'https://mybook.ru/api/booksets/{mybook_id}/',
                       headers={
                           'Accept': 'application/json;version=3'
                       },
                       timeout=5)
        response.raise_for_status()

        mybook_json = response.json()
        return cls.objects.create(mybook_json=mybook_json, mybook_id=mybook_id)


class HiddenBookset(models.Model):
    """
    This may look weird at first, but the idea is that it's easier to manage
    access this way.
    """
    bookset = models.ForeignKey(Bookset, on_delete=models.DO_NOTHING)

    @classmethod
    def from_mybook_id(cls, mybook_id):
        bookset = Bookset.from_mybook_id(mybook_id)
        return cls.objects.create(bookset=bookset)

    @property
    def json(self):
        return self.bookset.json


class Favorite(models.Model):
    owner = models.ForeignKey(User,
                              on_delete=models.DO_NOTHING,
                              related_name='owner',
                              related_query_name='owner')
    bookset = models.ForeignKey(Bookset, on_delete=models.DO_NOTHING)
