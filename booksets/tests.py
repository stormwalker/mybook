from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.

class BooksetsTest(TestCase):

    def test_view_renders_limit(self):
        response = self.client.get('/booksets/', )
        data = response.json()
        self.assertEqual(data['meta']['count'], 20)

    def test_editor_can_hide(self):
        editor = User.objects.create_superuser('editor', password='pass', is_staff=True, email='lol@example.com')

        self.client.login(username='editor', password='pass')

        response = self.client.get('/booksets/')
        drop_bookset_id = response.json()['objects'][0]['id']
        response = self.client.post('/booksets/hidden/', data={'mybook_id': drop_bookset_id})

        response = self.client.get('/booksets/')
        data = response.json()

        self.assertNotIn(drop_bookset_id, [obj['id'] for obj in data['objects']])

        response = self.client.get('/booksets/hidden/')
        data = response.json()
        self.assertIn(drop_bookset_id, [item['id'] for item in data['objects']])

    def test_user_can_fav(self):
        User.objects.create_user('jane', password='pass')

        self.client.login(username='jane', password='pass')

        response = self.client.get('/booksets/')
        jane_fav_bookset_id = response.json()['objects'][0]['id']
        bob_fav_bookset_id = response.json()['objects'][1]['id']
        self.client.post('/booksets/favorite/', data={'mybook_id': jane_fav_bookset_id})

        response = self.client.get('/booksets/favorite/')
        jane_fav_ids = [item['id'] for item in response.json()['objects']]
        self.assertIn(jane_fav_bookset_id, jane_fav_ids)
        self.assertNotIn(bob_fav_bookset_id, jane_fav_ids)

        User.objects.create_user('bob', password='pass')
        self.client.login(username='bob', password='pass')
        self.client.post('/booksets/favorite/', data={'mybook_id': bob_fav_bookset_id})
        response = self.client.get('/booksets/favorite/')
        bob_fav_ids = [item['id'] for item in response.json()['objects']]
        self.assertNotIn(jane_fav_bookset_id, bob_fav_ids)
        self.assertIn(bob_fav_bookset_id, bob_fav_ids)






