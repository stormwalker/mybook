from django.urls import path

from booksets.views import get_booksets, HiddenBooksetListView, FavoriteBooksetListView

urlpatterns = [
    path('', get_booksets, name='list-booksets'),
    path('hidden/', HiddenBooksetListView.as_view(), name='list-hidden-booksets'),
    path('favorite/', FavoriteBooksetListView.as_view(), name='list-favorite-booksets')
]
