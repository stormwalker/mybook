from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import HttpRequest, JsonResponse
# Create your views here.
from django.shortcuts import redirect
from django.views import View
from requests import get
from requests.exceptions import Timeout

from booksets.models import Bookset, Favorite, HiddenBookset


def get_booksets(request: HttpRequest):
    """
    Get list of booksets from mybook.ru
    """
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 20))

    hidden = HiddenBookset.objects.all().values_list('bookset_id', flat=True)
    hidden = list(hidden)
    objects = []

    # most likely will do one to two iterations
    while len(objects) < limit:
        try:
            response = get('https://mybook.ru/api/booksets/',
                           params={
                               'offset': offset,
                               'limit': limit
                           },
                           headers={
                               'Accept': 'application/json;version=3'
                           },
                           timeout=5)
        except Timeout:
            break
        data = response.json()
        for item in data['objects']:
            if len(objects) == limit:
                break
            if item['id'] not in hidden:
                objects.append(item)

        # if we reached end of list
        if len(data['objects']) < limit:
            break
        else:
            offset += limit

    meta = {
        'limit': limit,
        'count': len(objects)
    }
    return JsonResponse(data={'meta': meta, 'objects': objects}, status=200)


class HiddenBooksetListView(PermissionRequiredMixin, View):
    """
    get:
        show list of hidden booksets, which are not accessible at /booksets/
    post:
        hide bookset by mybook_id
    """
    permission_required = ('booksets.add_hidden', 'booksets.view_hidden')

    def get(self, request: HttpRequest):
        offset = int(request.GET.get('offset', 0))
        limit = int(request.GET.get('limit', 20))
        queryset = HiddenBookset.objects.all()[offset:offset + limit]

        objects = [bookset.json for bookset in queryset]
        data = {
            'meta': {
                'offset': offset,
                'limit': limit
            },
            'objects': objects
        }
        return JsonResponse(data=data, status=200)

    def post(self, request: HttpRequest):
        mybook_id = request.POST.get('mybook_id')
        HiddenBookset.from_mybook_id(mybook_id)
        return redirect('list-hidden-booksets')


class FavoriteBooksetListView(LoginRequiredMixin, View):

    def get(self, request: HttpRequest):
        offset = int(request.GET.get('offset', 0))
        limit = int(request.GET.get('limit', 20))
        owner = request.user
        queryset = Bookset.objects.filter(favorite__owner=owner)[offset:offset + limit]

        objects = [bookset.json for bookset in queryset]
        data = {
            'meta': {
                'offset': offset,
                'limit': limit
            },
            'objects': objects
        }
        return JsonResponse(data=data, status=200)

    def post(self, request: HttpRequest):
        mybook_id = request.POST.get('mybook_id')
        bookset = Bookset.from_mybook_id(mybook_id)
        owner = request.user
        Favorite.objects.create(owner=owner, bookset=bookset)
        return redirect('list-favorite-booksets')
