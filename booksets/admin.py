from django.contrib import admin

# Register your models here.
from booksets.models import HiddenBookset


@admin.register(HiddenBookset)
class HiddenBooksetAdmin(admin.ModelAdmin):
    pass