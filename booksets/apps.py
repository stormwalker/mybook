from django.apps import AppConfig


class BooksetsConfig(AppConfig):
    name = 'booksets'
